var gulp        = require('gulp');
//PHP Server
var php = require('gulp-connect-php');
var browserSync = require('browser-sync').create();
//Individuelle PHP Servereinstellungen
var phpServerData=require("../config.json");


// Static server
gulp.task('browser-sync', function() {
    php.server(
        phpServerData
    ,function(){
        browserSync.init({
            proxy:"127.0.0.1:8000",
            port:8000,
            open: true,
            notify: false
        });
    });
});