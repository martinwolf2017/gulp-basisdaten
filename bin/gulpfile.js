//Gulp
var gulp        = require('gulp');
//Include Task Folter
var requireDir = require('require-dir');
requireDir('./gulp-tasks');

var gulpRanInThisFolder = process.cwd();
gulp.task('default', ['browser-sync'], function () {
    gulp.watch(['**/*.php'], function(data){
        console.log(data.path.replace(gulpRanInThisFolder+'\\',""));
    });
});
